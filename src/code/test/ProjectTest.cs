﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todoist_AutomatizationMojix.src.code.page;

namespace Todoist_AutomatizationMojix.src.code.test
{
    [TestClass]
    public class ProjectTest : TestBase
    {
        MainPage mainPage = new MainPage();
        LoginPage loginPage = new LoginPage();
        ProjectPage projectPage = new ProjectPage();

        string username = "studioapollobolivia@gmail.com";
        string password = "Asdf_123456";
        string nameProjectList = "Prueba Lista";
        string editNameProjectList = "Prueba Lista 1";
        string nameProjectPanel = "Prueba Panel";
        string editNameProjectPanel = "Prueba Panel 1";

        [TestMethod]
        public void Test1_CreateListProject()
        {
            bool condition;
            login();
            Thread.Sleep(12000);

            projectPage.newProject.Click();
            projectPage.ChangeToModal();
            Thread.Sleep(2000);
            projectPage.nameProject.SetText(nameProjectList);
            projectPage.listTypeButton.Click();
            Thread.Sleep(1000);
            projectPage.createProject.Click();
            Thread.Sleep(1000);
            projectPage.FindProjectButton(nameProjectList);

            condition = projectPage.createListTask.IsControlDisplayed();
            Assert.IsTrue(condition, "ERROR !! List project wasn't create");
        }
        
        [TestMethod]
        public void Test2_CreatePaneltProject()
        {
            bool condition;
            login();
            Thread.Sleep(12000);

            projectPage.newProject.Click();
            projectPage.ChangeToModal();
            Thread.Sleep(2000);
            projectPage.nameProject.SetText(nameProjectPanel);
            projectPage.panelTypeButton.Click();
            Thread.Sleep(1000);
            projectPage.createProject.Click();
            Thread.Sleep(1000);
            projectPage.FindProjectButton(nameProjectPanel);

            if (projectPage.createPanelTask.IsControlDisplayed())
            {
                condition = true;
            }
            else if (projectPage.nameCreatePanelTask.IsControlDisplayed())
            {
                condition = true;
            }
            else
            {
                condition = false;
            }
            Assert.IsTrue(condition, "ERROR !! Panel project wasn't create");
        }

        [TestMethod]
        public void Test3_EditProject()
        {
            bool condition;
            login();
            Thread.Sleep(12000);

            projectPage.FindProjectButton(nameProjectList);
            projectPage.projectButton.Click();
            Thread.Sleep(100);
            projectPage.optionsProjectButton.Click();
            projectPage.editProjectButton.Click();
            projectPage.nameProject.SetText(editNameProjectList);
            projectPage.createProject.Click();
            Thread.Sleep(1000);

            projectPage.FindProjectButton(editNameProjectList);
            condition = projectPage.projectButton.IsControlDisplayed();
            Assert.IsTrue(condition, "ERROR !! Project wasn't update");
        }

        [TestMethod]
        public void Test4_DeleteProject()
        {
            bool condition;
            login();
            Thread.Sleep(12000);

            projectPage.FindProjectButton(editNameProjectList);
            projectPage.projectButton.Click();
            Thread.Sleep(100);
            projectPage.optionsProjectButton.Click();
            projectPage.deleteProjectButton.Click();
            projectPage.ChangeToModal();
            projectPage.confirmDeleteProjectButton.Click();
            Thread.Sleep(1000);

            condition = projectPage.projectButton.IsControlDisplayed();
            Assert.IsFalse(condition, "ERROR !! Project wasn't delete");
        }

        private void login()
        {
            mainPage.login.Click();
            Thread.Sleep(2000);
            loginPage.username.SetText(username);
            loginPage.password.SetText(password);
            loginPage.loginButton.Click();
        }
    }
}
