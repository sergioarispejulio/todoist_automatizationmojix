﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todoist_AutomatizationMojix.src.code.control;

namespace Todoist_AutomatizationMojix.src.code.page
{
    public class ProjectPage
    {
        public Button newProject = new Button(By.XPath("//div[@class='_2a3b75a1 d5d0d34a']/button"));
        public TextBox nameProject = new TextBox(By.XPath("//input[@id='edit_project_modal_field_name']"));
        public RadioButton listTypeButton = new RadioButton(By.XPath("//label[@id='project_list_view_style_option']/..//button[@aria-labelledby='project_list_view_style_option']"));
        public RadioButton panelTypeButton = new RadioButton(By.XPath("//label[@id='project_list_view_style_option']/..//button[@aria-labelledby='project_list_board_style_option']"));
        public Button createProject = new Button(By.XPath("//button[@class='_8313bd46 _7a4dbd5f _5e45d59f _2a3b75a1 _56a651f6']"));

        public Button projectButton = null;
        public Button optionsProjectButton = new Button(By.XPath("//div[@class='view_header__content']/div[@class='view_header__actions _2a3b75a1 _509a57b4 e5a9206f _50ba6b6b']/..//*[name()='g']/parent::*[name()='svg']/parent::button"));
        public Button editProjectButton = new Button(By.XPath("(//div[@class='_2a3b75a1 _509a57b4 _1fb9d90e _4a93668a _2580a74b'])[position()=1]/parent::li"));
        public Button deleteProjectButton = new Button(By.XPath("(//div[@class='_2a3b75a1 _509a57b4 _1fb9d90e _4a93668a _2580a74b'])[last()]/parent::li"));
        public Button confirmDeleteProjectButton = new Button(By.XPath("//button[@class='_8313bd46 _7a4dbd5f _5e45d59f _2a3b75a1 _56a651f6']"));

        //List type project
        public Button createListTask = new Button(By.XPath("//button[@class='plus_add_button']"));
        public TextBox nameCreateListTask = new TextBox(By.XPath("//form[@class='task_editor focus-marker-enabled-within customized_inline_wrapper']/..//div[@class='kr2Srm5 no-focus-marker task_editor__content_field--semibold']//div[@role='textbox']"));
        public Button createButtonListTask = new Button(By.XPath("//form[@class='task_editor focus-marker-enabled-within customized_inline_wrapper']/..//div[@data-testid='task-editor-action-buttons']/..//button[@data-testid='task-editor-submit-button']"));
        public Button cancelButtonListTask = new Button(By.XPath("//form[@class='task_editor focus-marker-enabled-within customized_inline_wrapper']/..//div[@data-testid='task-editor-action-buttons']/button[@aria-disabled='false']"));

        //Panel type project
        public Button createPanelTask = new Button(By.XPath("//div[@class='board_view__add_section']/button[@class='board_add_section_button']"));
        public TextBox nameCreatePanelTask = new TextBox(By.XPath("//div[@class='board_view__add_section']/form/input"));
        public Button createButtonPanelTask = new Button(By.XPath("//div[@class='board_view__add_section']/form/..//button[@type='submit']"));
        public Button cancelButtonPanelTask = new Button(By.XPath("//div[@class='board_view__add_section']/form/..//button[@class='cancel_link']"));


        public void FindProjectButton(string nameProject)
        {
            projectButton = new Button(By.XPath("//div[@id='left-menu-projects-panel']/..//span[contains(.,'"+ nameProject + "')]/parent::a/parent::div"));
        }

        public void ChangeToModal()
        {
            session.Session.Instance().GetBrowser().SwitchTo().ActiveElement();
        }

        public void ReturntoMainFrame()
        {
            session.Session.Instance().GetBrowser().SwitchTo().DefaultContent();
        }


    }
}
