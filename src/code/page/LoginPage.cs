﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todoist_AutomatizationMojix.src.code.control;

namespace Todoist_AutomatizationMojix.src.code.page
{
    public class LoginPage
    {
        public TextBox username = new TextBox(By.XPath("//input[@id='element-0']"));
        public TextBox password = new TextBox(By.XPath("//input[@id='element-3']"));
        public Button loginButton = new Button(By.XPath("//button[@data-gtm-id='start-email-login']"));
    }
}
