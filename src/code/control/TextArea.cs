﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todoist_AutomatizationMojix.src.code.control
{
    public class TextArea : ControlSelenium
    {
        public TextArea(By locator) : base(locator)
        {
        }

        public void SetText(String value)
        {
            FindControl();
            control.Clear();
            control.SendKeys(value);
        }
    }
}
